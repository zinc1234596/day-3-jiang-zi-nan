**ORID Template for Daily Report (2023/07/12)**

**Objective:**

- The objective of today's activities was to learn code review, Java8's Stream API, and OOP.

**Reflection:**

- During the code review phase, I learned the importance of communication in achieving the best results. I realized that different opinions within the team can lead to better code quality.
- Learning about Java8's Stream API helped me understand how to write more efficient code by replacing loops. This knowledge will be beneficial in improving my programming skills.
- The session on OOP provided a deeper understanding of encapsulation, inheritance, and polymorphism. But i didn't fully grasp the concepts.

**Interpretation:**

- I am not familiar enough with code review and relatively lack practice in OOP.

**Decision:**

- Code review helped me realize that I need to improve my code naming.
- I didn't fully grasp OOP due to lack of practice time, but I will work hard to improve my understanding in the future.
- 

  **In the review and extension after class, I focused on the following points:**
  1. It is important to have a general understanding of the intermediate and terminal operations in the Stream API, so that we can use them effectively in our work. It is recommended to prioritize remembering terminal operations such as forEach, min, max, reduce, count, and collect.
  2. Through OOP, I also had a basic understanding of how to use OOD to design logical relationships. And AOP is suitable for tasks such as logging operations, performance monitoring, and functional programming. For example, today we learned about Stream API and Lambda. And imperative programming like React.
  3. Comparing with what i previously learned about OOP in JS/TS, it is important to note that they don't strictly have ownership of OOP. Instead, they simulate it through prototypes and prototype chains. On the other hand, Lambda expressions in Java have similarities with anonymous arrow functions in JS/TS.
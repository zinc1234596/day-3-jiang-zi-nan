package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {

    private List<Klass> klassList = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String text = "";
        if (klassList.isEmpty()) return super.introduce() + String.format(" I am a teacher.");
        for (int i = 0; i < klassList.size(); i++) {
            if (i != klassList.size() - 1) {
                text += klassList.get(i).getNumber() + ", ";
            } else {
                text += klassList.get(i).getNumber() + ".";
            }
        }
        return super.introduce() + String.format(" I am a teacher. I teach Class %s", text);
    }

    public void assignTo(Klass klass) {
        if (!belongsTo(klass)) {
            klassList.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return klassList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return belongsTo(student.getKlass());
    }
}

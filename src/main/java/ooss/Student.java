package ooss;

public class Student extends Person {

    private Klass klass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public String introduce() {
        String text = "";
        if(klass != null){
            if(this.klass.isLeader(this)){
                text = String.format(" I am the leader of class %d.",klass.getNumber());
            }else{
                text = String.format(" I am in class %d.",klass.getNumber());
            }
        }
        return super.introduce() + String.format(" I am a student.%s", text);
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass){
        return this.klass == klass;
    }
}

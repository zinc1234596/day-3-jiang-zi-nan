package ooss;

import java.util.Objects;

public class Klass {
    private int number;

    private Student leader;

    private Person attachedPerson;

    public int getNumber() {
        return number;
    }


    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Klass)) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            this.leader = student;
            if (this.attachedPerson instanceof Teacher) {
                System.out.println("I am " + this.attachedPerson.getName() + ", teacher of Class " + this.number + ". I know " + student.getName() + " become Leader.");
            } else if (this.attachedPerson instanceof Student) {
                System.out.println("I am " + this.attachedPerson.getName() + ", student of Class " + this.number + ". I know " + student.getName() + " become Leader.");
            }
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        return leader == student;
    }

    public void attach(Person person) {
        this.attachedPerson = person;
    }
}
